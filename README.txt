
Description
-----------

Add Hupso Social Share Buttons (http://www.hupso.com/share) to your articles and
help visitors share your content on the most popular social networks:
Twitter, Facebook, Google+, Linkedin, StumbleUpon, Digg, Reddit, Bebo, Delicous.

Supported version: Drupal 7


Installation
------------

After install, enable the module from Modules -> Hupso Social Share Buttons


Configuration
-------------

Go to Configuration -> Hupso Social Share Buttons
To change CSS layout of share buttons edit file hupso.css
